#
# Page metadata for the base template
#
page_metadata:
  id: "team_work"
  title: "Lucid Dreams"

#
# Types of projects that should appear as "similar projects"
#
related_project_filters: ['udk']

#
# Heading breadcrumb options; each option must have a title, but can optionally specify a route.
#
breadcrumbs:
  - { title: "Portfolio" }
  - { title: "Team Work", url: "{{ path('_team_work') }}" }

#
# Developer data for the project
#
developer_metadata:
  # Fields for each developer metadata { display_text: "...", display_icon: "icon-...", display_inline: false, values: [{ is_label: false, label_type: ~, value: "..." }] }
  - display_text: "My Role"
    display_icon: "icon-user"
    values:
      - value: "Lead Programmer"

  - display_text: "Team Composition"
    display_icon: "icon-group"
    values:
      - value: "3 Programmers / 12 Developers"

  - display_text: "Development Lifecycle"
    display_icon: "icon-time"
    values:
      - value: "20 Weeks"

  - display_text: "Genre"
    display_icon: "icon-tags"
    display_inline: true
    values:
      - value: "First person"
        is_label: true
      - value: "<i class=\"icon-puzzle-piece\"></i>&nbsp;&nbsp;Puzzle"
        is_label: true
      - value: "Platformer"
        is_label: true

  - display_text: "Technology"
    display_icon: "icon-gears"
    values:
      - value: "<i class=\"icon-code\"></i>&nbsp;&nbsp;UnrealScript"
        is_label: true
        label_type: "primary"
      - value: "<img src=\"http://www.unrealengine.com/images/layout/udk.icon.png\" style=\"margin: 2px 3px 2px 0\"/> Unreal Development Kit"
        is_label: true

  - display_text: "Platform(s)"
    display_icon: "icon-laptop"
    values:
      - value: "<i class=\"icon-windows\"></i>&nbsp;&nbsp;Windows"
        is_label: true
        label_type: "primary"

#
# A brief description of the project. HTML allowed.
#
brief_overview: |
  <abbr title="">Lucid Dreams</abbr> is a first-person puzzle-platforming experience featuring a diverse selection of unique environments.
  Players solve puzzles by jumping, changing their perspective by rotating the world, or through the use of environmental
  hazards. Time trials and token collection provide an incentive for the player to skillfully beat levels.

#
#  A list of responsibilities for the project. HTML not allowed.
#
responsibilities:
  - Design and implementation of custom portal technology (render-to-texture, transformations).
  - Design and implementation of custom platform animation framework (using Cubic Hermite splines) to enable rapid development of complex platforming puzzles; node-based and editable through UnrealEd.
  - Design and implmentation of debugging framework (real-time observation of game object properties within a custom overlay).
  - Design and implementation of several Unreal Kismet nodes to promote productivity of non-programmers.
  - Design and implementation of several framebuffer effects using HLSL and Unreal Material Editor (chromatic abberation, per object highlighting, radial motion blur)
  - Design and implementation of audio framework (UnrealScript and supporting Unreal Kismet nodes) to simplify the process of adding gameplay-related sounds and music.
  - Assisted with implementation of graphical user interfaces using Scaleform.
  - Co-designed localization systems to facilitate on-demand changing of languages.
  - Authored technical design document.
  - Facilitated and encouraged cross-discipline communication during development.
  - Appropriate delegation of tasks among programming team.
  - Assigned bug tickets to appropriate personnel.
  - Creation of "patch notes" that collated feature additions, deprecations, modifications, and issue resolution.
  - Creation and maintenance of a centralized task list for the programming team within the development Wiki; provided more detail for each task and cross-referenced bug fixes.
  - Configuration and maintenance of development Wiki for documentation purposes.
  - Configuration and maintenance of Perforce version control server.

#
# Code sample data. HTML and twig allowed.
#
code_sample: ~

#
# Postmortem data
#
postmortem:
  developer_image:
    class: "pull-right" # "pull-left" or "pull-right"
    twig:
      URI: 'jcosite/img/portfolio/team/ld-postmortem1.png'

  # HTML content of the postmortem text
  body: |
    <h4>What Went Well</h4>
    <p>
    <ul class="responsibilities-list">
            <li><i class="icon-double-angle-right"></i> The overtime required during the second Module of development (“crunch time”) increased group cohesion and engendered a mutual respect amongst the team.</li>
            <li><i class="icon-double-angle-right"></i> Very consistent play testing during the redesign phase resulted in a very playable and fun experience overall.</li>
            <li><i class="icon-double-angle-right"></i> The redesign of game mechanics during the final Module resulted in a restructuring and more active cross-discipline game design regardless of title or role.</li>
    </ul>
    </p>
    <div class="top-pad"></div>
    <h4>What Went Wrong</h4>
    <p>
    <ul class="responsibilities-list">
            <li><i class="icon-double-angle-right"></i> Breakdown in communication during critical points in the development process.</li>
            <li><i class="icon-double-angle-right"></i> Lack of team buy-in for the initial concept of the game resulted in a lackluster and uninspired <strong>Vertical Slice</strong> submission.</li>
            <li><i class="icon-double-angle-right"></i> While we were reasonably vocal in communicating our concerns when game design had stagnated, we did not take enough steps to help the team as a whole progress past creative impasses.</li>
    </ul>
    </p>
    <div class="top-pad"></div>
    <h4>What We Learned</h4>
    <p>
    <ul class="responsibilities-list">
            <li><i class="icon-double-angle-right"></i> Championing even the smallest of tangible development progress results in <em>sizeable</em> boosts in morale and desire to continue across the team.</li>
            <li><i class="icon-double-angle-right"></i> Above all, communication and morale, are the most important aspects of team dynamics; without them, any idea—ingenious or otherwise—will not be found manifest in a “good game”.</li>
            <li><i class="icon-double-angle-right"></i> How to restart a project from the <em>very beginning</em> without giving up.</li>
    </ul>
    </p>

#
# Slide deck
#
slides:
  - backgroundURI: 'jcosite/img/portfolio/team/ld-1.png'
    transition2d: "all"
  - backgroundURI: 'jcosite/img/portfolio/team/ld-2.png'
    transition2d: "all"
  - backgroundURI: 'jcosite/img/portfolio/team/ld-3.png'
    transition2d: "all"