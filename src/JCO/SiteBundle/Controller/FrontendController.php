<?php

namespace JCO\SiteBundle\Controller;

use Symfony\Component\Yaml\Yaml;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontendController extends Controller
{
    public function indexAction()
    {
        $inflatedObject = Yaml::parse(file_get_contents(__DIR__ . '/../Resources/data/index.yml'));
        return $this->render('JCOSiteBundle:Frontend:main-page.html.twig', $inflatedObject);
    }

    public function resumeAction()
    {
        // Prepare template data required by Components\main-menu-base
        $templateData = array(
            "page_metadata" => array('id' => 'resume', 'title' => "Résumé")
        );

        return $this->render('JCOSiteBundle:Frontend:resume.html.twig', $templateData);
    }

    public function portfolioAction($type, $portfolioItem)
    {
        // Dispatch to the portfolio detail template if $portfolioItem is specified
        if (isset($portfolioItem))
        {
            // Parse correct YAML file for template metadata
            $inflatedObject = Yaml::parse(file_get_contents(__DIR__ . '/../Resources/data/Portfolio/Games/' . $portfolioItem . '.yml'));

            //!
            // Compute related project items based on filters in index data
            if (isset($inflatedObject['related_project_filters']))
            {
                // Initialize our related projects array
                $inflatedObject['similar_projects'] = array();

                // Parse the associated index data once
                $parsedIndexData = Yaml::parse(file_get_contents(__DIR__ . '/../Resources/data/Portfolio/' . $type . '-index.yml'));

                foreach ($parsedIndexData['portfolio_items'] as $project)
                {
                    // We should ignore considering ourselves...
                    if ($project['title'] != $inflatedObject['page_metadata']['title'])
                    {
                        // Attempt to locate the desired filter within the project found in the index.
                        // If it matches, then we add it to a similar_projects array, otherwise we ignore it...
                        foreach ($inflatedObject['related_project_filters'] as $desiredFilter)
                        {
                            if (in_array($desiredFilter, $project['filters']))
                            {
                                $inflatedObject['similar_projects'][] = $project;
                            }
                        }
                    }
                }
            }

            return $this->render('JCOSiteBundle:Frontend:portfolio-item-detail.html.twig', $inflatedObject);
        }

        // ... otherwise display the correct index page

        // Parse correct YAML file for template metadata
        $inflatedObject = Yaml::parse(file_get_contents(__DIR__ . '/../Resources/data/Portfolio/' . $type . '-index.yml'));

        return $this->render('JCOSiteBundle:Frontend:portfolio-index.html.twig', $inflatedObject);
    }

    public function thesisAction()
    {
        // Prepare template data required by Components\main-menu-base
        $templateData = array(
            "page_metadata" => array('id' => 'masters_thesis', 'title' => "Master's Thesis"),
            "breadcrumbs"   => array(
                array('title' => 'Portfolio')
            )
        );

        return $this->render('JCOSiteBundle:Frontend:thesis.html.twig', $templateData);
    }

    public function aboutAction()
    {
        // Prepare template data required by Components\main-menu-base
        $templateData = array(
            "page_metadata" => array('id' => 'about_me', 'title' => "About Me")
        );

        return $this->render('JCOSiteBundle:Frontend:about-me.html.twig', $templateData);
    }
}
