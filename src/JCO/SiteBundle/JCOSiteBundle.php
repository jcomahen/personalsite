<?php
/**
 * The bundle for my personal website.
 */
namespace JCO\SiteBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class JCOSiteBundle extends Bundle
{
}