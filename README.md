Personal Website
========================

Welcome to the repository for my personal website! This is an example of a very simple data-driven website.
The site runs on PHP 5.5 and uses Symfony as the backend framework to facilitate rapid development; there is
no database present. All data is stored in a flat-file format using YAML and results are cached for rapid retrieval.